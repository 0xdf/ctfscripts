from pwn import *
import sys
from base64 import b64decode


def get_hex_from_base64(b64str):
    with open('elf', 'wb') as f:
        f.write(b64decode(b64str))

    e = ELF('elf', checksec=False)
    lea_addr = e.entrypoint + 4
    # log.success(f"Got lea addr: {hex(lea_addr)}")
    buf_offset = u32(e.read(lea_addr + 3, 4), sign='signed')
    # log.success(f'Got buffer offset: {hex(buf_offset)}')
    buf_addr = e.entry + 4 + 7 + buf_offset
    values = e.read(buf_addr, 24)
    # log.success(f'Got values: {values.hex()}')
    return values.hex()


r = remote(*(sys.argv[1].split(':')))

with log.progress("Solving...") as p:
    for i in range(129):
        r.recvuntil(b'ELF:  ')
        base64str = r.recvline()
        result = get_hex_from_base64(base64str)
        r.sendline(result.encode())
        p.status(f"Solved {i+1} of 129")
    
print(r.recvall().decode().strip())