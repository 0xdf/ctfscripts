// ==UserScript==
// @name         HolidayHack - Ent Locations
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       0xdf
// @match        https://2022.kringlecon.com/
// @match        https://2023.holidayhackchallenge.com/
// @match        https://2024.holidayhackchallenge.com/
// @icon         https://www.google.com/s2/favicons?sz=64&domain=kringlecon.com
// @grant        GM_addStyle
// ==/UserScript==

GM_addStyle('#locations {position: fixed; top: 50px; left: 10px; padding: 10px; width: 400px; z-index: 10; background: black; border-radius: 15px; opacity: 0.5}');
GM_addStyle('.pos {width: 50px; display: inline-block;}');
GM_addStyle('.door-clicker.one::before { content: "⭐"; font-size: xx-large;}');


(function() {
    'use strict';

    window.setInterval(locations, 2000);
})();

function locations() {
    var locations = document.getElementById('locations');
    var list;
    if (!locations) {
        locations = document.createElement('div');
        locations.id = "locations";

        var root = document.getElementById('root');
        root.insertBefore(locations, root.firstChild);
    }

    list = document.createElement('ul');
    var ents = document.getElementsByClassName('ent');
    Array.from(ents).forEach((ent) => {
        var dispName = Array.from(ent.classList).filter(c => !['ent', 'player', 'npc', 'type-terminal'].includes(c))[0]
        if (dispName && !['marker', 'hat', 'veteran', 'walking'].some((word) => dispName.startsWith(word))) {
            if (ent.querySelector('.player-username')) {
                dispName = ent.querySelector('.player-username').innerText;
            } else if (ent.querySelector('.entity-name')) {
                dispName = ent.querySelector('.entity-name').innerText;
            } else if (ent.querySelector('.npc-username')) {
                dispName = ent.querySelector('.npc-username').innerText;
            }
            var loc = ent.getAttribute('data-location');
            var li = document.createElement('li');
            var span = document.createElement('span');
            span.classList.add('pos');
            span.appendChild(document.createTextNode('[' + loc + ']'));
            li.appendChild(span);
            li.appendChild(document.createTextNode(dispName));
            list.appendChild(li)
        }
    });
    var doors = document.querySelectorAll('.door-clicker.one');
    Array.from(doors).forEach((door) => {
        var dispName = door.classList[2];
        var li = document.createElement('li');
        var span = document.createElement('span');
        li.appendChild(span);
        li.appendChild(document.createTextNode(dispName));
        list.appendChild(li)
    });
    locations.innerHTML = '';
    locations.appendChild(list);
}
