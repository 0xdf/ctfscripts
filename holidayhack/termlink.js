// ==UserScript==
// @name         HoldayHack - Add Terminal Link
// @namespace    http://tampermonkey.net/
// @version      2024-11-11
// @description  Add link to open terminals in new tab
// @author       0xdf
// @match        https://2024.holidayhackchallenge.com/
// @icon         https://www.google.com/s2/favicons?sz=64&domain=kringlecon.com
// @grant        none
// ==/UserScript==

(function() {
    window.onload = function() {
        const observer = new MutationObserver((mutationsList) => {
            for (const mutation of mutationsList) {
                mutation.addedNodes.forEach((node) => {
                    if (node.nodeType === Node.ELEMENT_NODE && node.classList.contains('challenge')) {
                        addAnchorToDiv(node);
                    }
                });
            }
        });
        observer.observe(document.body, { childList: true, subtree: true });
    };
})();

function addAnchorToDiv(div) {
    const anchor = document.createElement('a');
    anchor.href = div.querySelector('iframe').src;
    anchor.target = '_blank';
    anchor.style.position = 'absolute';
    anchor.style.bottom = '10px';
    anchor.style.right = '10px';
    anchor.style.padding = '5px';
    anchor.style.backgroundColor = '#007bff';
    anchor.style.color = '#fff';
    anchor.style.borderRadius = '4px';
    anchor.style.textDecoration = 'none';
    anchor.style.zIndex = '10';

    anchor.textContent = "Term in new tab  ";
    const icon = document.createElement('i');
    icon.classList.add('fas', 'fa-external-link-square-alt');
    anchor.appendChild(icon);

    div.appendChild(anchor);
}

